import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		double rückgabebetrag=fahrkartenBezahlen(fahrkartenBestellungErfassen());
		fahrkartenAusgeben();
		rückgeldAusgeben(rückgabebetrag);
	}

	public static double fahrkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double preis;
		int anzahl;

		System.out.print("Ticketpreis in €: ");
		preis = tastatur.nextDouble();
		System.out.print("Anzahl Tickets: ");
		anzahl = tastatur.nextInt();
		zuZahlenderBetrag = preis * anzahl;
		tastatur.close();
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze = 0;
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			while (eingeworfeneMünze == 0) {
				System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
				eingeworfeneMünze = tastatur.nextDouble();
				if (eingeworfeneMünze < 0.05 || eingeworfeneMünze > 2) {
					System.out.println("Münze darf nicht weniger als 5ct oder mehr als 2€ sein.");
					eingeworfeneMünze = 0;
				}
			}
			eingezahlterGesamtbetrag += eingeworfeneMünze;
			eingeworfeneMünze = 0;
		}
		tastatur.close();
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rückgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}