
public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Dies ist \"ein Beispielsatz\"\n");
		System.out.println("Ein Beispielsatz ist das");
		// println fügt automatisch \n an das Ende des zu druckenden Strings hinzu
		System.out.println("      *\r\n" + "     ***\r\n" + "    *****\r\n" + "   *******\r\n" + "  *********\r\n"
				+ " ***********\r\n" + "*************\r\n" + "     ***\r\n" + "     ***");
		Double[] numbers = { 22.4234234, 111.2222, 4.0, 1000000.551, 97.34 };
		for (double i : numbers) {
			System.out.printf("%.2f%n", i);
		}
		System.out.println("  * *\r\n" + "*     *\r\n" + "*     *\r\n" + "  * *");
		System.out.printf("%-5s= %-19s=%4s%n","0!","","1");
		System.out.printf("%-5s= %-19s=%4s%n","1!","1","1");
		System.out.printf("%-5s= %-19s=%4s%n","2!","1 * 2","2");
		System.out.printf("%-5s= %-19s=%4s%n","3!","1 * 2 * 3","6");
		System.out.printf("%-5s= %-19s=%4s%n","4!","1 * 2 * 3 * 4","24");
		System.out.printf("%-5s= %-19s=%4s%n","5!","1 * 2 * 3 * 4 * 5","120");
		
		System.out.printf("%-12s|%10s%n","Fahrenheit","Celsius");
		System.out.printf("%s%n","------------------------");
		System.out.printf("%-12s|%10.2f%n",-20,-28.8889);
		System.out.printf("%-12s|%10.2f%n",-10,-23.3333);
		System.out.printf("%-12s|%10.2f%n",0,-17.7778);
		System.out.printf("%-12s|%10.2f%n",20,-6.6667);
		System.out.printf("%-12s|%10.2f%n",30,-1.1111);
	}
}
